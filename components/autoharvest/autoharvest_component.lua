local AutoharvestComponent = class()
local HARVEST_ACTION = 'stonehearth:harvest_renewable_resource'
-- local renewable_resource_node = self._entity:get_component('stonehearth:renewable_resource_node')
-- local task_tracker = self._entity:get_component('stonehearth:task_tracker')
-- local owner_id = self._entity:get_player_id()

function AutoharvestComponent:initialize()
   local json = radiant.entities.get_json(self)

   self._sv.enable = false
   self._sv.last_owner = nil
   -- non-saved variables.
   self._json = json
   self._task_cancelled_listener = nil
   self._renew_listener = nil
   self._player_id_trace = nil
end

function AutoharvestComponent:destroy()
   self:_destroy_renew_listener()
   self:_destroy_task_cancelled_listener()
   self:_destroy_player_id_trace()
   local commands_component = self._entity:get_component('stonehearth:commands')
   if commands_component then
      commands_component:remove_command('autoharvest_mod:commands:autoharvest_on')
      commands_component:remove_command('autoharvest_mod:commands:autoharvest_off')
   end
end

function AutoharvestComponent:restore()
   --for backward compatibilty
   local _sv_changed = false
   if self._sv.enable == nil then
      self._sv.enable = false
      _sv_changed = true
   end
   if self._sv.last_owner == nil then
      local owner_id = ''
      self._sv.last_owner = pcall(function() owner_id = self._entity:get_player_id() end) and owner_id or ''
      _sv_changed = true
   end
   if _sv_changed then
      self.__saved_variables:mark_changed()
   end
end

function AutoharvestComponent:post_activate()
   local renewable_resource_node = self._entity:get_component('stonehearth:renewable_resource_node')
   local commands_component = self._entity:add_component('stonehearth:commands')
   if radiant.is_server then
      if not renewable_resource_node then
         --print('AH post_activate() without RRN')
         commands_component:remove_command('autoharvest_mod:commands:autoharvest_on')
         commands_component:set_command_enabled('autoharvest_mod:commands:autoharvest_off', false)
      elseif renewable_resource_node._json and renewable_resource_node._json.spawn_resource_immediately then
         --print('AH post_activate() with spawn_resource_immediately set to true')
         commands_component:remove_command('autoharvest_mod:commands:autoharvest_on')
         commands_component:set_command_enabled('autoharvest_mod:commands:autoharvest_off', false)
      elseif self._sv.enable == true then
         --print('AH post_activate() with AH enabled')
         self:activate_autoharvest(self._sv.last_owner)
      else
         --print('AH post_activate() with AH disabled')
         self:deactivate_autoharvest(self._sv.last_owner, true)
      end
      self:_create_player_id_trace()
   end
end

function AutoharvestComponent:is_active()
   return self._sv.enable
end

function AutoharvestComponent:activate_autoharvest(player_id, force_regardless_of_player_id)
   --print('Activating AH, player_id: ' .. player_id)
   local owner_id = ''
   local current_owner = pcall(function() owner_id = self._entity:get_player_id() end) and owner_id or ''
   if (force_regardless_of_player_id == true) or (player_id == current_owner) then
      local result = self._entity:get_component('stonehearth:renewable_resource_node'):request_harvest(player_id)
      if result then
         self:_create_task_cancelled_listener()
      else
         self:_create_renew_listener()
      end
      local commands_component = self._entity:add_component('stonehearth:commands')
      commands_component:remove_command('autoharvest_mod:commands:autoharvest_off')
      commands_component:add_command('autoharvest_mod:commands:autoharvest_on')
      self._sv.enable = true
      self._sv.last_owner = player_id
      self.__saved_variables:mark_changed()
      return true
   else
      return false
   end
end

function AutoharvestComponent:deactivate_autoharvest(player_id, force_regardless_of_player_id)
   --print('Deactivating AH, player_id: ' .. player_id)
   local owner_id = ''
   local current_owner = pcall(function() owner_id = self._entity:get_player_id() end) and owner_id or ''
   if (force_regardless_of_player_id == true) or (player_id == current_owner) then
      self:_destroy_renew_listener()
      self:_destroy_task_cancelled_listener()
      local commands_component = self._entity:add_component('stonehearth:commands')
      commands_component:remove_command('autoharvest_mod:commands:autoharvest_on')
      commands_component:add_command('autoharvest_mod:commands:autoharvest_off')
      self._sv.enable = false
      self._sv.last_owner = player_id
      self.__saved_variables:mark_changed()
      return true
   else
      return false
   end
end

function AutoharvestComponent:_create_renew_listener()
   self._renew_listener = radiant.events.listen(self._entity, 'stonehearth:on_renewable_resource_renewed', self, self._on_renew)
end

function AutoharvestComponent:_destroy_renew_listener()
   if self._renew_listener then
      self._renew_listener:destroy()
      self._renew_listener = nil
   end
end

function AutoharvestComponent:_create_task_cancelled_listener()
   self._task_cancelled_listener = radiant.events.listen(self._entity, 'autoharvest_mod:task_tracker:task_cancelled', self, self._on_task_cancelled)
end

function AutoharvestComponent:_destroy_task_cancelled_listener()
   if self._task_cancelled_listener then
      self._task_cancelled_listener:destroy()
      self._task_cancelled_listener = nil
   end
end

function AutoharvestComponent:_create_player_id_trace()
   self._player_id_trace = self._entity:trace_player_id('autoharvest component tracking player_id')
      :on_changed(function()
         self:_on_player_id_changed()
      end)
end

function AutoharvestComponent:_destroy_player_id_trace()
   if self._player_id_trace then
      self._player_id_trace:destroy()
      self._player_id_trace = nil
   end
end

function AutoharvestComponent:_on_renew()
   if self._sv.enable then
      if not radiant.entities.is_different_non_empty_player_id(self._entity:get_player_id(), self._sv.last_owner) then
         local renewable_resource_node = self._entity:get_component('stonehearth:renewable_resource_node')
         if renewable_resource_node then
            self:_destroy_renew_listener()
            renewable_resource_node:request_harvest(self._sv.last_owner)
            self:_create_task_cancelled_listener()
            return true
         else
            return false
         end
      else
         return false
      end
   else
      return false
   end
end

function AutoharvestComponent:_on_task_cancelled(args)
   --local owner_id = self._entity:get_player_id()
   if args.task_activity_name == HARVEST_ACTION and args.player_id == self._sv.last_owner then
      self:deactivate_autoharvest(args.player_id)
   end
end

function AutoharvestComponent:_on_player_id_changed()
   local owner_id = self._entity:get_player_id()
   --print('AH detected player ID change to: ' .. owner_id)
   --radiant.entities.is_different_non_empty_player_id(owner_id, self._sv.last_owner) -- doesn't work with ownerless RRNs and task cancelling
   if owner_id ~= self._sv.last_owner then
      -- self._sv.last_owner = owner_id -- deactivate_autoharvest() will do it anyway
      self:deactivate_autoharvest(owner_id, true)
   end
end

return AutoharvestComponent