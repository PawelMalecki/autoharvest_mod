local Point3 = _radiant.csg.Point3
local Cube3 = _radiant.csg.Cube3
local Color4 = _radiant.csg.Color4
-- local entity_forms_lib = require 'stonehearth.lib.entity_forms.entity_forms_lib'
local validator = radiant.validator

local AutoharvestCallHandler = class()

local boxed_entities = {}

function AutoharvestCallHandler:toggle_autoharvest(session, response, entity)
   local renewable_resource_node = entity:get_component('stonehearth:renewable_resource_node')
   if renewable_resource_node then
      if entity:get_player_id() == '' then
         radiant.entities.set_player_id(entity, session.player_id)
      end
      local autoharvest = entity:add_component('autoharvest_mod:autoharvest')
      if autoharvest:is_active() then
         autoharvest:deactivate_autoharvest(session.player_id)
      else
         autoharvest:activate_autoharvest(session.player_id)
      end
   else -- remove autoharvest commands if the target isn't a renewable resource node.
      local commands_component = entity:get_component('stonehearth:commands')
      if commands_component then
         commands_component:remove_command('autoharvest_mod:commands:autoharvest_on')
         commands_component:remove_command('autoharvest_mod:commands:autoharvest_off')
      end
   end
end

function AutoharvestCallHandler:box_autoharvest(session, response)
   stonehearth.selection:select_xz_region(stonehearth.constants.xz_region_reasons.BOX_HARVEST_RESOURCES)
      :set_max_size(50)
      :require_supported(true)
      :use_outline_marquee(Color4(0, 255, 0, 32), Color4(0, 255, 0, 255))
      :set_cursor('stonehearth:cursors:zone_generic')
      -- Allow selection on buildings/other items that aren't selectable
      :allow_unselectable_support_entities(true)
      :set_find_support_filter(function(result)
            if self:_is_ground(result.entity) then
               return true
            end
            return stonehearth.selection.FILTER_IGNORE
         end)
      :done(function(selector, box)
            _radiant.call('autoharvest_mod:server_box_autoharvest', box)
            response:resolve(true)
         end)
      :fail(function(selector)
            response:reject('no region')
         end)
      :go()
end

function AutoharvestCallHandler:server_box_autoharvest(session, response, box)
   validator.expect_argument_types({'Cube3'}, box)

   local cube = Cube3(Point3(box.min.x, box.min.y, box.min.z),
                      Point3(box.max.x, box.max.y, box.max.z))

   local entities = radiant.terrain.get_entities_in_cube(cube)

   for _, entity in pairs(entities) do
      self:autoharvest_entity(session, response, entity, true) -- true for from harvest tool
   end
end

function AutoharvestCallHandler:autoharvest_entity(session, response, entity, from_harvest_tool)
   validator.expect_argument_types({'Entity', validator.optional('boolean')}, entity, from_harvest_tool)   

   local town = stonehearth.town:get_town(session.player_id)

   local renewable_resource_node = entity:get_component('stonehearth:renewable_resource_node')
   local commands_component = entity:get_component('stonehearth:commands')

   if renewable_resource_node and commands_component:has_command('autoharvest_mod:commands:autoharvest_off') then
      self:toggle_autoharvest(session, response, entity)
   end
end

-- returns true if the entity should be considered a target when box selecting items
function AutoharvestCallHandler:_is_ground(entity)
   if entity:get_component('terrain') then
      return true
   end
   
   if (entity:get_component('stonehearth:construction_data') or
       entity:get_component('stonehearth:build2:structure')) then
      return true
   end

   return false
end

return AutoharvestCallHandler