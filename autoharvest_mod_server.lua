autoharvest_mod = {}

local monkey_patches = {}

local conditional_patches = {
   stonehearth_ace = {
      enabled = {
         autoharvest_renewable_resource_node_component = 'stonehearth.components.renewable_resource_node.renewable_resource_node_component',
         autoharvest_task_tracker_component = 'stonehearth.components.task_tracker.task_tracker_component'
      },
      disabled = {
         autoharvest_renewable_resource_node_component = 'stonehearth.components.renewable_resource_node.renewable_resource_node_component',
         autoharvest_task_tracker_component = 'stonehearth.components.task_tracker.task_tracker_component'
      }
   }
}

function autoharvest_mod.setup_patching()
   for key, value in pairs(conditional_patches) do
      if autoharvest_mod.is_mod_loaded(key) then
         radiant.events.listen(radiant, key .. ':server:required_loaded', autoharvest_mod, function()
            radiant.log.write_('autoharvest_mod', 0, 'Autoharvest server monkey-patching after ' .. key)
            autoharvest_mod.monkey_patching(value.enabled)
         end)
      else
         radiant.log.write_('autoharvest_mod', 0, 'Autoharvest server did not detect ' .. key)
         for from, into in pairs(value.disabled) do
            monkey_patches[from] = into
         end
      end
   end
   radiant.events.listen(radiant, 'radiant:required_loaded', autoharvest_mod, autoharvest_mod._on_required_loaded)
end

function autoharvest_mod.monkey_patching(patches)
   for from, into in pairs(patches) do
      local monkey_see = require('monkey_patches.' .. from)
      local monkey_do = radiant.mods.require(into)
      radiant.log.write_('autoharvest_mod', 0, 'Autoharvest server monkey-patching sources \'' .. from .. '\' => \'' .. into .. '\'')
      if monkey_see.MERGE_PATCH_INTO_TABLE then
         -- use merge_into_table to also mixin other values, not just functions
         radiant.util.merge_into_table(monkey_do, monkey_see)
      else
         radiant.mixin(monkey_do, monkey_see)
      end
   end
end

function autoharvest_mod.is_mod_loaded(namespace)
   for _, mod in ipairs(radiant.resources.get_mod_list()) do
      if mod == namespace then
         return true
      end
   end
   return false
end

function autoharvest_mod:_on_init()
   radiant.log.write_('autoharvest_mod', 0, 'Autoharvest (v2.3b) server initialized')
   autoharvest_mod.setup_patching()
end

function autoharvest_mod:_on_required_loaded()
   autoharvest_mod.monkey_patching(monkey_patches)
   radiant.events.trigger_async(radiant, 'autoharvest_mod:server:required_loaded')
end

radiant.events.listen(autoharvest_mod, 'radiant:init', autoharvest_mod, autoharvest_mod._on_init)

return autoharvest_mod
