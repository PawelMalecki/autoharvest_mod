local AceRenewableResourceNodeComponent = require 'stonehearth_ace.monkey_patches.ace_renewable_resource_node_component'
local AutoharvestRenewableResourceNodeComponent = class()

AutoharvestRenewableResourceNodeComponent._autoharvest_backup_spawn_resource = AceRenewableResourceNodeComponent.spawn_resource

function AutoharvestRenewableResourceNodeComponent:spawn_resource(harvester_entity, location, owner_player_id)
   local autoharvest = self._entity:get_component('autoharvest_mod:autoharvest')
   local result
   if autoharvest then
      autoharvest:_destroy_task_cancelled_listener()
      result = self:_autoharvest_backup_spawn_resource(harvester_entity, location, owner_player_id)
      autoharvest:_create_renew_listener()
   else
      result = self:_autoharvest_backup_spawn_resource(harvester_entity, location, owner_player_id)
   end
   return result
end

return AutoharvestRenewableResourceNodeComponent