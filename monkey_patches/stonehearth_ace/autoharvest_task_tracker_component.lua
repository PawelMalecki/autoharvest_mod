local AceTaskTrackerComponent = require 'stonehearth_ace.monkey_patches.ace_task_tracker_component'
local ResponsiveTaskTrackerComponent = class()

ResponsiveTaskTrackerComponent._autoharvest_old_cancel_current_task = AceTaskTrackerComponent.cancel_current_task

function ResponsiveTaskTrackerComponent:cancel_current_task(should_reconsider_ai)
   local args = {
      entity = self._entity,
      player_id = self._sv.task_player_id,
      category = self._sv.task_category,
      task_activity_name = self._sv.task_activity_name,
      task_effect_name = self._sv._task_effect_name,
	  cancelled_if_entity_moves = self._sv._cancelled_if_entity_moves
   }
   local result = self:_autoharvest_old_cancel_current_task(should_reconsider_ai)
   if result then
      radiant.events.trigger(self._entity, 'autoharvest_mod:task_tracker:task_cancelled', args)
   end
   return result
end

return ResponsiveTaskTrackerComponent
