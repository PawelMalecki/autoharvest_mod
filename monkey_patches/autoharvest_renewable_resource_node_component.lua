local RenewableResourceNodeComponent = require 'stonehearth.components.renewable_resource_node.renewable_resource_node_component'
local AutoharvestRenewableResourceNodeComponent = class()

AutoharvestRenewableResourceNodeComponent._autoharvest_backup_initialize = RenewableResourceNodeComponent.initialize
function AutoharvestRenewableResourceNodeComponent:initialize()
   self:_autoharvest_backup_initialize()

   local json = radiant.entities.get_json(self)
   -- Save variables
   self._sv.autoharvest_mod_disable = json['autoharvest_mod:disable'] or json['ignore_auto_harvest'] or false
end

AutoharvestRenewableResourceNodeComponent._autoharvest_backup_post_activate = RenewableResourceNodeComponent.post_activate
function AutoharvestRenewableResourceNodeComponent:post_activate()
   self:_autoharvest_backup_post_activate()

   if not self._sv.autoharvest_mod_disable then
      self._entity:add_component('autoharvest_mod:autoharvest')
   end
end

AutoharvestRenewableResourceNodeComponent._autoharvest_backup_spawn_resource = RenewableResourceNodeComponent.spawn_resource
function AutoharvestRenewableResourceNodeComponent:spawn_resource(harvester_entity, location, owner_player_id)
   local autoharvest = self._entity:get_component('autoharvest_mod:autoharvest')
   local result
   if autoharvest then
      autoharvest:_destroy_task_cancelled_listener()
      result = self:_autoharvest_backup_spawn_resource(harvester_entity, location, owner_player_id)
      autoharvest:_create_renew_listener()
   else
      result = self:_autoharvest_backup_spawn_resource(harvester_entity, location, owner_player_id)
   end
   return result
end

return AutoharvestRenewableResourceNodeComponent