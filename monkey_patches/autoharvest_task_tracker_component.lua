local TaskTrackerComponent = require 'stonehearth.components.task_tracker.task_tracker_component'
local ResponsiveTaskTrackerComponent = class()

-- local log = radiant.log.create_logger('task_tracker')

--[[
ResponsiveTaskTrackerComponent._autoharvest_old_request_task = TaskTrackerComponent.request_task
function ResponsiveTaskTrackerComponent:request_task(player_id, category, task_activity_name, task_effect_name)
   local result = self:_autoharvest_old_request_task(player_id, category, task_activity_name, task_effect_name)
   if result then
      radiant.events.trigger(self._entity, 'responsive_task_tracker:task_requested', {entity = self._entity, player_id = player_id, category = category, task_activity_name = task_activity_name, task_effect_name = task_effect_name})
   end
   return result
end
]]--

ResponsiveTaskTrackerComponent._autoharvest_old_cancel_current_task = TaskTrackerComponent.cancel_current_task

function ResponsiveTaskTrackerComponent:cancel_current_task(should_reconsider_ai)
   local args = {
      entity = self._entity,
      player_id = self._sv.task_player_id,
      category = self._sv.task_category,
      task_activity_name = self._sv.task_activity_name,
      task_effect_name = self._sv._task_effect_name,
	  cancelled_if_entity_moves = self._sv._cancelled_if_entity_moves
   }
   local result = self:_autoharvest_old_cancel_current_task(should_reconsider_ai)
   if result then
      radiant.events.trigger(self._entity, 'autoharvest_mod:task_tracker:task_cancelled', args)
   end
   return result
end

return ResponsiveTaskTrackerComponent
