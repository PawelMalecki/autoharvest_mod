App.StonehearthStartMenuView.reopen({
   init: function() {
      var self = this;

      self.menuActions.box_autoharvest = function(){
         self.boxAutoharvest();
      };

      self._super();

      App.waitForStartMenuLoad().then(() => {
         // this is a call to a global function stored in task_manager.js
         _updateProcessingMeterShown();
      });
   },

   boxAutoharvest: function() {
      var self = this;

      var tip = App.stonehearthClient.showTip('autoharvest_mod:ui.game.menu.harvest_menu.items.box_autoharvest.tip_title',
            'autoharvest_mod:ui.game.menu.harvest_menu.items.box_autoharvest.tip_description', {i18n : true});

      return App.stonehearthClient._callTool('boxAutoharvest', function() {
         return radiant.call('autoharvest_mod:box_autoharvest')
            .done(function(response) {
               radiant.call('radiant:play_sound', {'track' : 'stonehearth:sounds:ui:start_menu:popup'} );
               self.boxAutoharvest();
            })
            .fail(function(response) {
               App.stonehearthClient.hideTip(tip);
            });
      });
   }
});